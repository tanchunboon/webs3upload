variable "new_subnet_prefix_length" {
  type    = number
  default = 24  # Change to your desired prefix length
}

variable "ami_id" {
  type    = string
  default = "ami-0464f90f5928bccb8" #Amazon Linux 2023 AMI
}

variable "app_name" {
  type    = string
  default = "demowebapp"
}

variable "environment" {
  type    = string
  default = "dev"
}