data "aws_vpc" "default" {
  default = true
}

data "aws_region" "current" {}

data "aws_availability_zones" "az" {
    state = "available"
}

data "aws_subnet" "default" {
    filter {
    name  = "vpc-id"
    values = [data.aws_vpc.default.id]
    }
}

resource "aws_subnet" "private_subnet" {
  count = "${length(data.aws_availability_zones.az.names)}"
  vpc_id     = data.aws_vpc.default.id
  cidr_block = cidrsubnet(data.aws_vpc.default.cidr_block, var.new_subnet_prefix_length, count.index + 2)
  availability_zone = "${data.aws_availability_zones.az.names[count.index]}"

  tags = {
    Name = "Private Subnet ${count.index + 1}"
  }
}


resource "aws_security_group" "alb_sg" {
  // Define your ALB security group rules here
  name        = "alb_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = [data.aws_vpc.default.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_security_group" "ec2_sg" {
  name        = "ec2_sg"
  // Define your EC2 security group rules here
    egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "allow_alb" {
  type              = "ingress"
  to_port           = 80
  protocol          = "tcp"
  source_security_group_id = aws_security_group.alb_sg.id
  from_port         = 80
  security_group_id = aws_security_group.ec2_sg.id
}


resource "aws_lb" "webapp" {
  name               = "${var.app_name}.${var.environment}-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups   = [aws_security_group.alb_sg.id]
    dynamic "subnet_mapping" {
    for_each = aws_subnet.private_subnet.*.id
    content {
      subnet_id = subnet_mapping.value
    }
}
}

resource "aws_launch_configuration" "webapp" {
  name_prefix          = "${var.app_name}.${var.environment}-lc"
  image_id             = var.ami_id
  instance_type        = "t2.micro"  # Choose an appropriate instance type
  security_groups     = [aws_security_group.ec2_sg.id]
  user_data = <<-EOT
              #!/bin/bash
              yum install -y nginx aws-cli
              service nginx start

              # Add cron job to pull files from private S3 bucket
              echo '*/5 * * * * aws s3 sync s3://your-private-bucket /var/www/html' >> /var/spool/cron/root
              EOT
}

resource "aws_autoscaling_group" "webapp" {
  name                  = "${var.app_name}.${var.environment}-asg"
  launch_configuration  = aws_launch_configuration.webapp.name
  min_size              = 2
  max_size              = 5
  desired_capacity      = 2
    # dynamic "subnet_mapping" {
    # for_each = aws_subnet.private_subnet.*.id
    # content {
    #   vpc_zone_identifier = subnet_mapping.value
    # }
  vpc_zone_identifier   =  [aws_subnet.private_subnet[*].id]
}


resource "aws_vpc_endpoint" "s3" {
  vpc_id            = data.aws_vpc.default.id
  service_name      = "com.amazonaws.ap-southeast-1.s3"
}

resource "aws_s3_bucket" "private" {
  bucket = "${var.app_name}${var.environment}privatebucketname"
}

resource "aws_s3_bucket_acl" "private" {
  depends_on = [aws_s3_bucket_ownership_controls.private]
  bucket = aws_s3_bucket.private.id
  acl    = "private"
}

resource "aws_s3_bucket_ownership_controls" "private" {
  bucket = aws_s3_bucket.private.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_nat_gateway" "private" {
  subnet_id     = data.aws_subnet.default.id
}

resource "aws_route_table" "private_route_table" {
  vpc_id = data.aws_vpc.default.id
}

resource "aws_route" "private_route" {
  route_table_id         = aws_route_table.private_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.private.id
}