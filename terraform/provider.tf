provider "aws" {
  region = "ap-southeast-1"
  default_tags {
   tags = {
     Environment = var.environment
     AppName     = var.app_name
   }
}
}